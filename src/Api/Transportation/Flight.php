<?php

namespace Api\Transportation;

use Api\Transportation\AbstractTransport;

class Flight extends AbstractTransport
{
    /**
     * Constructor for Train
     */
    public function __construct()
    {

    }

    /**
     * Return part of the message for itinerary
     *
     * @return  string $message
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getMessage()
    {
        $msg = 'From {@ORIGIN}, take flight '.$this->getName();
        $msg .=' to {@DESTINATION}. Gate {@GATE}';
        $seat = $this->getSeat();
        if ($seat) {
            $msg .= ',seat '.$seat;
        } else {
            $msg .= ',no seat assignment';
        }
        return $msg;
    }
}
