<?php

namespace Api\Transportation;

use Api\Transportation\AbstractTransport;

class Train extends AbstractTransport
{
    /**
     * Constructor for Train
     */
    public function __construct()
    {

    }

    /**
     * Return part of the message for itinerary
     *
     * @return  string $message
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getMessage()
    {
        $msg = 'Take train '.$this->getName().' from {@ORIGIN} to {@DESTINATION}. ';
        $seat = $this->getSeat();
        if ($seat) {
            $msg .= 'Sit in seat '.$seat;
        } else {
            $msg .= 'No seat assignment';
        }
        return $msg;
    }
}
