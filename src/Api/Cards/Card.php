<?php

namespace Api\Cards;

use Api\Places\Place;
use Api\Transportation\AbstractTransport;
use Api\Cards\AbstractCard;

/**
 *
 */
class Card extends AbstractCard
{
    /**
     * Card's constructor
     *
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function __construct()
    {

    }

    /**
     *
     * @return string
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getItinerary()
    {
        $msg = $this->getTransport()->getMessage();

        $msg = str_replace(
            ['{@ORIGIN}','{@DESTINATION}','{@GATE}'],
            [   $this->getOrigin()->getName(),
                $this->getDestination()->getName(),
                $this->getGate()
            ],
            $msg
        );

        if ($this->getBaggageMessage()) {
            $msg .= '. '.$this->getBaggageMessage();
        }

        return $msg;
    }
}
