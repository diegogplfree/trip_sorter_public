<?php

namespace Api\Cards;

use Api\Places\Place;
use Api\Transportation\AbstractTransport;
use Api\Transportation\Train;
use Api\Transportation\Bus;
use Api\Transportation\Flight;
use Api\Cards\Card;

/**
 *
 */
class CardFactory
{

    /**
     * [getCard description]
     * @param  string            $gate           [description]
     * @param  string            $baggageMessage [description]
     * @param  Place             $origin         [description]
     * @param  Place             $destination    [description]
     * @param  AbstractTransport $transport      [description]
     *
     * @return AbstractCard $card
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getCard(
        string $gate,
        string $baggageMessage,
        Place $origin,
        Place $destination,
        AbstractTransport $transport
    ) {
        $card = new Card();
        $card->setGate($gate);
        $card->setBaggageMessage($baggageMessage);
        $card->setOrigin($origin);
        $card->setDestination($destination);
        $card->setTransport($transport);
        return $card;
    }

    /**
     *
     * @param  string $jsonCard [description]
     * @return AbstractCard $card
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function buildCard(array $card)
    {
        if (is_array($card) &&
            isset($card['transport']) &&
            isset($card['transport']['type']) &&
            isset($card['transport']['seat']) &&
            isset($card['transport']['name']) &&
            isset($card['origin']) &&
            isset($card['origin']['name']) &&
            isset($card['origin']['id']) &&
            isset($card['destination']) &&
            isset($card['destination']['name']) &&
            isset($card['destination']['id']) &&
            isset($card['gate']) &&
            isset($card['baggageMessage'])
            ) {
            //transport
            switch ($card['transport']['type']) {
                case 'train':
                    $transport = new Train();
                    break;
                case 'bus':
                    $transport = new Bus();
                    break;
                case 'flight':
                    $transport = new Flight();
                    break;
            }
            $transport->setSeat($card['transport']['seat']);
            $transport->setName($card['transport']['name']);

            //origin
            $origin = new Place();
            $origin->setName($card['origin']['name']);
            $origin->setId($card['origin']['id']);

            //destination
            $destination = new Place();
            $destination->setName($card['destination']['name']);
            $destination->setId($card['destination']['id']);

            return $this->getCard(
                $card['gate'],
                $card['baggageMessage'],
                $origin,
                $destination,
                $transport
            );
        }
    }
}
