<?php

namespace Api\Cards;

use Api\Places\Place;
use Api\Transportation\AbstractTransport;
use Api\Cards\CardInterface;

/**
 *
 */
abstract class AbstractCard implements CardInterface
{

    /**
     * Origin
     * @var Place
     */
    protected $origin;

    /**
     * Destination
     * @var Place
     */
    protected $destination;

    /**
     * Transportation
     * @var AbstractTransport
     */
    protected $transport;

    /**
     * Gate
     * @var string
     */
    protected $gate;

    /**
     * Baggage message
     * @var string
     */
    protected $baggageMessage;

    /**
     * Set Card's origin
     *
     * @param Place $origin [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setOrigin(Place $origin)
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * Get Card's origin
     *
     * @return  Place $origin
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * Set Card's destination
     *
     * @param Place $destination [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setDestination(Place $destination)
    {
        $this->destination = $destination;
        return $this;
    }

    /**
     * Get Card's destination
     *
     * @return  Place $origin
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set Card's transport
     *
     * @param Transport $transport [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setTransport(AbstractTransport $transport)
    {
        $this->transport = $transport;
        return $this;
    }

    /**
     * Get Card's transport
     *
     * @return  Transport $transport
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * Set Card's gate
     *
     * @param string $gate [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setGate(string $gate)
    {
        $this->gate = $gate;
        return $this;
    }

    /**
     * Get Card's gate
     *
     * @return  string $gate
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getGate()
    {
        return $this->gate;
    }

    /**
     * Set Card's baggageMessage
     *
     * @param string $baggageMessage [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setBaggageMessage(string $baggageMessage)
    {
        $this->baggageMessage = $baggageMessage;
        return $this;
    }

    /**
     * Get Card's baggageMessage
     *
     * @return  string $baggageMessage
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getBaggageMessage()
    {
        return $this->baggageMessage;
    }

    public function getItinerary()
    {

    }
}
