<?php

namespace Api\Cards;

use Api\Places\Place;
use Api\Transportation\AbstractTransport;

/**
 *
 */
interface CardInterface
{
    /**
     * Set Card's origin
     *
     * @param Place $origin [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setOrigin(Place $origin);

    /**
     * Get Card's origin
     *
     * @return  Place $origin
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getOrigin();

    /**
     * Set Card's destination
     *
     * @param Place $destination [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setDestination(Place $destination);

    /**
     * Get Card's destination
     *
     * @return  Place $origin
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getDestination();

    /**
     * Set Card's transport
     *
     * @param Transport $transport [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setTransport(AbstractTransport $transport);

    /**
     * Get Card's transport
     *
     * @return  Transport $transport
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getTransport();

    /**
     * Set Card's gate
     *
     * @param string $gate [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setGate(string $gate);

    /**
     * Get Card's gate
     *
     * @return  string $gate
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getGate();

    /**
     * Set Card's baggageMessage
     *
     * @param string $baggageMessage [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setBaggageMessage(string $baggageMessage);

    /**
     * Get Card's baggageMessage
     *
     * @return  string $baggageMessage
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getBaggageMessage();
}
