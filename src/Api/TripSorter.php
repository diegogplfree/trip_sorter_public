<?php

namespace Api;

use Api\Cards\CardFactory;

/**
 *
 */
class TripSorter
{
    /**
     * TripSorter's constructor
     *
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function __construct()
    {

    }

    /**
     *
     * @param  array  $unsortCards a matrix of array with format
     *  [
     *       'transport' => [
     *           'type' => 'bus',
     *           'seat' => '',
     *           'name' => 'airport'
     *       ],
     *       'origin' => [
     *           'name' => 'Madrid',
     *           'id' => '15'
     *       ],
     *       'destination' => [
     *           'name' => 'Barcelona',
     *           'id' => '1'
     *       ],
     *       'gate' => '',
     *       'baggageMessage' => ''
     *   ]
     * @return array $itinerary array of string with the itinerary
     */
    public function sort(array $unsortCards)
    {
        $cards = [];
        $factory = new CardFactory();
        foreach ($unsortCards as $tmpCard) {
            $cards[] = $factory->buildCard($tmpCard);
        }

        $sortCards = $this->mergesort($cards);

        $itinerary = [];
        foreach ($sortCards as $tmpCard) {
            $itinerary[] = $tmpCard->getItinerary();
        }

        return $itinerary;
    }

    /**
     * [mergesort description]
     * @param  [type] $cards [description]
     * @return [type]              [description]
     */
    public function mergesort($listUnsortCards)
    {
        if (count($listUnsortCards) == 1) {
            return $listUnsortCards;
        }

        $merged[] = array_pop($listUnsortCards);

        $merged = $this->searchOriginDestination($merged, $listUnsortCards);

        while (count($merged)>0 && count($listUnsortCards)>0) {
            $merged = $this->searchOriginDestination($merged, $listUnsortCards);
        }

        return $merged;
    }

    /**
     * [searchOriginDestination description]
     * @param  [type] $origin       [description]
     * @param  [type] &$listUnsortCards [description]
     * @return [type]               [description]
     */
    public function searchOriginDestination($origin, &$listUnsortCards)
    {
        $chainTrip = [];
        $nextFound = -1;
        $previousFound = -1;

        foreach ($listUnsortCards as $key => $dest) {
            if ($previousFound==-1 &&
                $origin[0]->getOrigin()->getId() == $dest->getDestination()->getId()
            ) {
                if ($nextFound==-1) {
                    $chainTrip = array_merge($origin, $chainTrip);
                }
                $chainTrip = array_merge(array($dest), $chainTrip);
                $previousFound = $key;
            }
            if ($nextFound==-1 && $origin[count($origin)-1]->getDestination()->getId() == $dest->getOrigin()->getId()) {
                if ($previousFound==-1) {
                    $chainTrip = array_merge($origin, $chainTrip);
                }
                $chainTrip[] = $dest;
                $nextFound = $key;
            }

            if ($nextFound!=-1 && $previousFound!=-1) {
                break;
            }
        }
        if ($nextFound!=-1) {
            unset($listUnsortCards[$nextFound]);
        }

        if ($previousFound!=-1) {
            unset($listUnsortCards[$previousFound]);
        }
        return $chainTrip;
    }
}
