To execute tests:

1. Goto root Folder (TripSorter)
2. Execute command: composer install
3. Execute command: phpunit tests/

In the file TripSorter/tests/Api/TripSorterTest.php is an example of how to use the Api.

NOTE: This Api do not support json or xml format because that is the responsability of another layer of the application.