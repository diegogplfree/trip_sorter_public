<?php

namespace Api\Transportation;

/**
 *
 */
class FlightTest extends \PHPUnit_Framework_TestCase
{
    const NAME = 'BC78';
    const SEAT = 'A02';

    /**
     * Test Flight's constructor
     *
     * @return  Flight $flight
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testFlight()
    {
        $flight = new Flight();
        $this->assertInstanceOf(Flight::class, $flight);
        return $flight;
    }

    /**
     * @depends testFlight
     *
     * @return Flight $flight
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testSetName(Flight $flight)
    {
        $this->assertInstanceOf(Flight::class, $flight->setName(self::NAME));
        return $flight;
    }

    /**
     * @depends testSetName
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetName(Flight $flight)
    {
        $this->assertEquals(self::NAME, $flight->getName());
    }

    /**
     * @depends  testSetName
     * @param  Flight  $flight
     * @return Flight $flight
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testSetSeat(Flight $flight)
    {
        $this->assertInstanceOf(Flight::class, $flight->setSeat(self::SEAT));
        return $flight;
    }

    /**
     * @depends testSetSeat
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetSeat(Flight $flight)
    {
        $this->assertEquals(self::SEAT, $flight->getSeat());
    }

    /**
     * @depends testSetSeat
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetMessage(Flight $flight)
    {
        $message = $flight->getMessage();
        $this->assertStringStartsWith('From ', $message);
    }
}
