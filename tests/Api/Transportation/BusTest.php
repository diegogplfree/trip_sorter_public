<?php

namespace Api\Transportation;

/**
 *
 */
class BusTest extends \PHPUnit_Framework_TestCase
{
    const NAME = 'airport';
    const SEAT = '';

    /**
     * Test Bus's constructor
     *
     * @return  Bus $bus
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testBus()
    {
        $bus = new Bus();
        $this->assertInstanceOf(Bus::class, $bus);
        return $bus;
    }

    /**
     * @depends testBus
     *
     * @return Bus $bus
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testSetName(Bus $bus)
    {
        $this->assertInstanceOf(Bus::class, $bus->setName(self::NAME));
        return $bus;
    }

    /**
     * @depends testSetName
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetName(Bus $bus)
    {
        $this->assertEquals(self::NAME, $bus->getName());
    }

    /**
     * @depends  testSetName
     * @param  Bus  $bus
     * @return Bus $bus
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testSetSeat(Bus $bus)
    {
        $this->assertInstanceOf(Bus::class, $bus->setSeat(self::SEAT));
        return $bus;
    }

    /**
     * @depends testSetSeat
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetSeat(Bus $bus)
    {
        $this->assertEquals(self::SEAT, $bus->getSeat());
    }

    /**
     * @depends testSetSeat
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetMessage(Bus $bus)
    {
        $message = $bus->getMessage();
        $this->assertStringStartsWith('Take the', $message);
    }
}
