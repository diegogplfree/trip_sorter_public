<?php

namespace Api;

use Api\TripSorter;

/**
 *
 */
class TripSorterTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Test constructor
     *
     * @return  TripSorter $factory
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testTripSorter()
    {
        $tripSorter = new TripSorter();
        $this->assertInstanceOf(TripSorter::class, $tripSorter);
        return $tripSorter;
    }

    /**
     * [sortTest description]
     * @depends testTripSorter
     * @return [type] [description]
     */
    public function testSort(TripSorter $tripSorter)
    {
        $unsortCards[] =  [
            'transport' => [
                'type' => 'train',
                'seat' => '12A',
                'name' => '78A'
            ],
            'origin' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'destination' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'gate' => '24',
            'baggageMessage' => ''
        ];

        $unsortCards[] =  [
            'transport' => [
                'type' => 'bus',
                'seat' => '',
                'name' => 'airport'
            ],
            'origin' => [
                'name' => 'Madrid',
                'id' => '15'
            ],
            'destination' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'gate' => '',
            'baggageMessage' => ''
        ];

        $unsortCards[] =  [
            'transport' => [
                'type' => 'flight',
                'seat' => '7B',
                'name' => 'SK22'
            ],
            'origin' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'destination' => [
                'name' => 'Stockholm',
                'id' => '20'
            ],
            'gate' => '22',
            'baggageMessage' => 'Baggage will we automatically transferred from your last leg'
        ];

        $listCards = $tripSorter->sort($unsortCards);
    }

    /**
     * [sortTest description]
     * @depends testTripSorter
     * @return [type] [description]
     */
    public function testSortSingle(TripSorter $tripSorter)
    {
        $unsortCards[] =  [
            'transport' => [
                'type' => 'train',
                'seat' => '12A',
                'name' => '78A'
            ],
            'origin' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'destination' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'gate' => '24',
            'baggageMessage' => ''
        ];

        $listCards = $tripSorter->sort($unsortCards);
    }

    /**
     * [sortTest description]
     * @depends testTripSorter
     * @return [type] [description]
     */
    public function testSortNext(TripSorter $tripSorter)
    {
        $unsortCards[] =  [
            'transport' => [
                'type' => 'train',
                'seat' => '12A',
                'name' => '78A'
            ],
            'origin' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'destination' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'gate' => '24',
            'baggageMessage' => ''
        ];

        $unsortCards[] =  [
            'transport' => [
                'type' => 'flight',
                'seat' => '7B',
                'name' => 'SK22'
            ],
            'origin' => [
                'name' => 'Gerona Airport',
                'id' => '2'
            ],
            'destination' => [
                'name' => 'Stockholm',
                'id' => '20'
            ],
            'gate' => '22',
            'baggageMessage' => 'Baggage will we automatically transferred from your last leg'
        ];

        $unsortCards[] =  [
            'transport' => [
                'type' => 'bus',
                'seat' => '',
                'name' => 'airport'
            ],
            'origin' => [
                'name' => 'Madrid',
                'id' => '15'
            ],
            'destination' => [
                'name' => 'Barcelona',
                'id' => '1'
            ],
            'gate' => '',
            'baggageMessage' => ''
        ];

        $itinerary = $tripSorter->sort($unsortCards);

        var_dump($itinerary);
    }
}
