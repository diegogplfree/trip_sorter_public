<?php

namespace Api\Cards;

use Api\Places\Place;
use Api\Transportation\AbstractTransport;

/**
 *
 */
class CardTest extends \PHPUnit_Framework_TestCase
{

    const GATE = '45B';
    const BAGGAGE_MESSAGE = 'Baggage will we automatically transferred from your last leg.';

    /**
     * Test Card's constructor
     *
     * @return  Card $card
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testCard()
    {
        $card = new Card();
        $this->assertInstanceOf(Card::class, $card);
        return $card;
    }

    /**
     * Test setOrigin
     *
     * @depends testCard
     *
     * @return Card $card
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testSetOrigin(Card $card)
    {
        $place = $this->prophesize(Place::class);
        $this->assertInstanceOf(Card::class, $card->setOrigin($place->reveal()));
        return $card;
    }

    /**
     * @depends testSetOrigin
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetOrigin(Card $card)
    {
        $this->assertInstanceOf(Place::class, $card->getOrigin());
    }

    /**
     * Test testSetOrigin
     *
     * @depends testCard
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     * @return Card $card
     */
    public function testSetDestination(Card $card)
    {
        $place = $this->prophesize(Place::class);
        $this->assertInstanceOf(Card::class, $card->setDestination($place->reveal()));
        return $card;
    }

    /**
     * @depends testSetDestination
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetDestination(Card $card)
    {
        $this->assertInstanceOf(Place::class, $card->getDestination());
    }

    /**
     * Test testSetDestination
     *
     * @depends testCard
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     * @return Card $card
     */
    public function testSetTransport(Card $card)
    {
        $transport = $this->prophesize(AbstractTransport::class);
        $this->assertInstanceOf(Card::class, $card->setTransport($transport->reveal()));
        return $card;
    }

    /**
     * @depends testSetDestination
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetTransport(Card $card)
    {
        $this->assertInstanceOf(AbstractTransport::class, $card->getTransport());
    }

    /**
     * Test testSetTransport
     *
     * @depends testCard
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     * @return Card $card
     */
    public function testSetGate(Card $card)
    {
        $this->assertInstanceOf(Card::class, $card->setGate(self::GATE));
        return $card;
    }

    /**
     * @depends testSetGate
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetGate(Card $card)
    {
        $this->assertEquals(self::GATE, $card->getGate());
    }

    /**
     * Test testSetGate
     *
     * @depends testCard
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     * @return Card $card
     */
    public function testSetBaggageMessage(Card $card)
    {
        $this->assertInstanceOf(Card::class, $card->setBaggageMessage(self::BAGGAGE_MESSAGE));
        return $card;
    }

    /**
     * @depends testSetBaggageMessage
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetBaggageMessage(Card $card)
    {
        $this->assertEquals(self::BAGGAGE_MESSAGE, $card->getBaggageMessage());
    }

    /**
     * @depends testSetBaggageMessage
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetItinerary(Card $card)
    {
        $message = $card->getItinerary();
        $this->assertNotEmpty($message);
    }
}
