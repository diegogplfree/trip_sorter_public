<?php

namespace Api\Places;

/**
 *
 */
class PlaceTest extends \PHPUnit_Framework_TestCase
{
    const PLACE_NAME = 'Berlin';
    const PLACE_ID = 1;

    /**
     * Test Place's constructor
     *
     * @return  Place $place
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testPlace()
    {
        $place = new Place();
        $this->assertInstanceOf(Place::class, $place);
        return $place;
    }

    /**
     * @depends testPlace
     *
     * @return Place $place
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testSetName(Place $place)
    {
        $this->assertInstanceOf(Place::class, $place->setName(self::PLACE_NAME));
        return $place;
    }

    /**
     * @depends testSetName
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetName(Place $place)
    {
        $this->assertEquals(self::PLACE_NAME, $place->getName());
    }

    /**
     * @depends  testSetName
     * @param  Place  $place
     * @return Place $place
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testSetId(Place $place)
    {
        $this->assertInstanceOf(Place::class, $place->setId(self::PLACE_ID));
        return $place;
    }

    /**
     * @depends testSetId
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetId(Place $place)
    {
        $this->assertEquals(self::PLACE_ID, $place->getId());
    }
}
